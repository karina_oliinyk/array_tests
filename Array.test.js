const Array = require('./index');

describe('Check for existence methods in class Array', () => {

  test('Checking method "map"', () => {
    expect(Array.prototype.map).toBeDefined();
  });

  test('Checking method "reduce"', () => {
    expect(Array.prototype.reduce).toBeDefined();
  });

  test('Checking method "toString"', () => {
    expect(Array.prototype.toString).toBeDefined();
  });

  test('Checking method "some"', () => {
    expect(Array.prototype.some).toBeDefined();
  });

  test('Checking method "indexOf"', () => {
    expect(Array.prototype.indexOf).toBeDefined();
  });  
});
