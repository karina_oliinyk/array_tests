# TESTS FOR METHOD OF ARRAY

## Task
Write tests for some Array's methods: 
- map
- toString 
- reduce
- some
- indexOf
- and for Array's instance.

## The different types of tests
**Unit tests**
Unit tests are very low level, close to the source of your application. They consist in testing individual methods and functions of the classes, components or modules used by your software. Unit tests are in general quite cheap to automate and can be run very quickly by a continuous integration server.

**Integration tests**
Integration tests verify that different modules or services used by your application work well together. For example, it can be testing the interaction with the database or making sure that microservices work together as expected. These types of tests are more expensive to run as they require multiple parts of the application to be up and running.

**Functional tests**
Functional tests focus on the business requirements of an application. They only verify the output of an action and do not check the intermediate states of the system when performing that action.

There is sometimes a confusion between integration tests and functional tests as they both require multiple components to interact with each other. The difference is that an integration test may simply verify that you can query the database while a functional test would expect to get a specific value from the database as defined by the product requirements.


## For creating tests we were using JS library - JEST
Tutorial JEST


## Install Jest using npm:
```html
npm install --save-dev jest
```

## Example JEST test:
```html
function sum(a, b) {
return a + b;
}
module.exports = sum;

test('adds 1 + 2 to equal 3', () => {
expect(sum(1, 2)).toBe(3);
});
```

## How to run your JEST test:

Write in terminal: 
```html
npx jest <filename.test.js>
```

